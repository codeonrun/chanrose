let [counter, primary, secondary] = [0, "#342E37", "#FEFBF3"];

const myStorage = window.localStorage;
if (myStorage.hasOwnProperty("counter"))
  counter = Number(myStorage.getItem("counter"));
else myStorage.setItem("counter", 0);

function setCSSVar(key, val) {
  document.documentElement.style.setProperty(key, val);
}

function displayTheme() {
  if (counter % 2) {
    console.log("Dark Theme Activated!");
    setCSSVar("--primary", secondary);
    setCSSVar("--secondary", primary);
  } else {
    console.log("Light Theme Activated!");
    setCSSVar("--secondary", secondary);
    setCSSVar("--primary", primary);
  }
}

function runMain() {
  counter++;
  myStorage.setItem("counter", counter);
  displayTheme();
}

displayTheme();
